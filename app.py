from config import FLASK_ENV, config

from db import db

from flask import Flask

from routes import apply_routes  # noqa: F401,E402, circular dependency


def create_app(env):
    app = Flask(__name__, static_folder='static/')
    app.config.from_object(config[env])
    db.init_app(app)
    apply_routes(app)
    return app


app = create_app(FLASK_ENV)
