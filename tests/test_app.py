import unittest

from app import create_app

from db import db

from models.trade import Trade


def create_trades(db):
    trades = [
        Trade(
            buy_currency='USD',
            sell_currency='EUR',
            buy_amount_cents=50000,
            sell_amount_cents=63313,
            rate=1.2756,
        ),
        Trade(
            buy_currency='GBP',
            sell_currency='EUR',
            buy_amount_cents=50000,
            sell_amount_cents=58500,
            rate=1.1700,
        ),
    ]

    db.session.bulk_save_objects(trades)
    db.session.commit()


class TestIntegration(unittest.TestCase):
    def setUp(self):
        self.app = create_app('test')
        self.app_context = self.app.app_context()
        self.app_context.push()

        db.create_all()
        create_trades(db)

        self.client = self.app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_root_path(self):
        response = self.client.get('/')

        assert 'text/html' in response.headers['Content-Type']
        assert '<!DOCTYPE html>' in str(response.get_data())
        assert 'eBury Trading System' in str(response.get_data())

    def test_index(self):
        response = self.client.get('/index.html')

        assert 'text/html' in response.headers['Content-Type']
        assert '<!DOCTYPE html>' in str(response.get_data())
        assert 'eBury Trading System' in str(response.get_data())

    def test_js_assets(self):
        response = self.client.get('/js/script.js')

        assert 'application/javascript' in response.headers['Content-Type']
        assert '$(document).ready(function()' in str(response.get_data())

    def test_css_assets(self):
        response = self.client.get('/css/main.css')

        assert 'text/css' in response.headers['Content-Type']
        assert 'body {' in str(response.get_data())

    def test_trades_resource_endpoint(self):
        response = self.client.get('/api/trades')

        assert 'application/json' in response.headers['Content-Type']
        assert type(response.get_json()) is list
        assert len(response.get_json()) == 2

        assert response.get_json()[0]['buy_currency'] == 'USD'
        assert response.get_json()[0]['buy_amount_cents'] == 50000
        assert response.get_json()[0]['sell_currency'] == 'EUR'
        assert response.get_json()[0]['sell_amount_cents'] == 63313
        assert response.get_json()[0]['rate'] == 1.2756

        assert response.get_json()[1]['buy_currency'] == 'GBP'
        assert response.get_json()[1]['buy_amount_cents'] == 50000
        assert response.get_json()[1]['sell_currency'] == 'EUR'
        assert response.get_json()[1]['sell_amount_cents'] == 58500
        assert response.get_json()[1]['rate'] == 1.1700
