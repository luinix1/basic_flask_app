import unittest
from datetime import datetime, timedelta

from app import create_app

from db import db

from models.trade import Trade


def within_two_seconds_from_now(moment):
    delta = datetime.now() - moment
    return delta <= timedelta(seconds=2)


class TestTradeModel(unittest.TestCase):
    def setUp(self):
        self.app = create_app('test')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_can_persist_trade(self):
        trade = Trade(
                    buy_currency='USD',
                    sell_currency='EUR',
                    buy_amount_cents=50000,
                    sell_amount_cents=63313,
                    rate=1.2756,
                )
        db.session.add(trade)
        db.session.commit()

        assert Trade.query.count() == 1

        persisted_trade = Trade.query.get(trade.id)
        assert trade.id.startswith('TR')
        assert len(trade.id) == 9
        assert persisted_trade.buy_currency == 'USD'
        assert persisted_trade.sell_currency == 'EUR'
        assert persisted_trade.buy_amount_cents == 50000
        assert persisted_trade.sell_amount_cents == 63313
        assert persisted_trade.rate == 1.2756
        assert within_two_seconds_from_now(persisted_trade.date_booked)
