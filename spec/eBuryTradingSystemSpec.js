describe("eBury Trading System", function() {
  navigator = { language: "en-US" }; // Using a global variable is not the best way of doing this
  window = { alert: jasmine.createSpy('mockAlert') } // Same here

  var app = require("../static/js/script");

  var elementMock = jasmine.createSpyObj("elemMock", ["html", "show", "hide", "trigger"]);
  var jqueryMock = jasmine.createSpy("jqueryMock").and.returnValue(elementMock);

  var tradesFixture = [
    {
    id: "TR428YR1O",
    sell_currency: "USD",
    sell_amount_cents: 50000,
    buy_currency: "EUR",
    buy_amount_cents: 63313,
    rate: 1.2756,
    date_booked: "2018/05/27 23:13:05"
    },
    {
      id: "TR428YR11",
      sell_currency: "EUR",
      sell_amount_cents: 63313,
      buy_currency: "USD",
      buy_amount_cents: 50000,
      rate: 1.2756,
      date_booked: "2018/05/27 23:15:09"
    }
  ];

  var expectedTradesHtml = '<div class="trades-table-row"><div class="table-body-cell">USD</div><div class="table-body-cell">$500.00</div><div class="table-body-cell">EUR</div><div class="table-body-cell">€633.13</div><div class="table-body-cell">1.2756</div><div class="table-body-cell">2018/05/27 23:13:05</div></div><div class="trades-table-row"><div class="table-body-cell">EUR</div><div class="table-body-cell">€633.13</div><div class="table-body-cell">USD</div><div class="table-body-cell">$500.00</div><div class="table-body-cell">1.2756</div><div class="table-body-cell">2018/05/27 23:15:09</div></div>'; 

  beforeEach(function() {
    jqueryMock.ajax = function(params) { params.success(tradesFixture); }
    spyOn(jqueryMock, "ajax").and.callThrough();
  });

  describe("getTrades", function() {
    it("translates the trades from the API to HTML", function() {

      app(jqueryMock).getTrades();
      expect(jqueryMock).toHaveBeenCalledWith("#trades-table-body");
      expect(elementMock.html).toHaveBeenCalledWith(expectedTradesHtml);
    });
  });

  describe("addTrade", function() {
    it("shows a form for adding a new trade", function() {
      app(jqueryMock).newTrade();
      expect(jqueryMock).toHaveBeenCalledWith("#new-trade-form");
      expect(elementMock.show).toHaveBeenCalledWith();
    });
  });

  describe("saveTrade", function() {
    var eventMock = jasmine.createSpyObj("eventMock", ["preventDefault"]);

    var buyCurrencyMock = jasmine.createSpyObj("buyCurrencyMock", { val: 'GBP' });
    var sellCurrencyMock = jasmine.createSpyObj("sellCurrencyMock", { val: 'EUR' });
    var rateMock = jasmine.createSpyObj("rateMock", { val: 1.17 });
    var buyAmountMock = jasmine.createSpyObj("buyAmountMock", { val: 500 });
    var sellAmountMock = jasmine.createSpyObj("sellAmountMock", { val: 585.15 });

    var jqueryMock;

    beforeEach(function() {
      jqueryMock = function(selector) {
        return {
          "#buy-currency": buyCurrencyMock,
          "#sell-currency": sellCurrencyMock,
          "#rate": rateMock,
          "#buy-amount": buyAmountMock,
          "#sell-amount": sellAmountMock,
          "#new-trade-form": elementMock,
          "#trades-table-body": elementMock
        }[selector]
      }

      jqueryMock.post = function(url, data, callback) { callback(); };
      spyOn(jqueryMock, "post").and.callThrough();

      jqueryMock.ajax = function(params) { params.success(tradesFixture); }
      spyOn(jqueryMock, "ajax").and.callThrough();
    });

    it("prevents the browser from submitting the form", function() {
      app(jqueryMock).saveTrade(eventMock);
      expect(eventMock.preventDefault).toHaveBeenCalledWith();
    });

    it("sends the correct data", function() {
      app(jqueryMock).saveTrade(eventMock);
      expect(jqueryMock.post).toHaveBeenCalledWith(
        'api/trades',
        {
          buy_currency: "GBP",
          sell_currency: "EUR",
          rate: 1.17,
          buy_amount_cents: 50000,
          sell_amount_cents: 58515
        },
        jasmine.any(Function)
      );
    });

    it("alerts in case of success, hides the form, clears all fields and refreshes the list", function() {
      app(jqueryMock).saveTrade(eventMock);
      expect(window.alert).toHaveBeenCalledWith("New trade created successfully");

      expect(elementMock.hide).toHaveBeenCalledWith();

      expect(elementMock.trigger).toHaveBeenCalledWith("reset");

      expect(elementMock.html).toHaveBeenCalledWith(expectedTradesHtml);
    });
  });

  describe("getRate", function() {
    var response = {
      rates: {
        USD: 0.0092139883
      },
      base:"JPY",
      date:"2020-02-03"
    };

    var exchangeRatesApiUrl = "https://api.exchangeratesapi.io/latest";

    beforeEach(function() {
      jqueryMock.get = function(url, data, callback) { callback(response); };
      spyOn(jqueryMock, "get").and.callThrough();
    });

    it("uses exchangeratesapi for finding the current exchange rate between two currencies", function(done) {
      app(jqueryMock).getRate('JPY', 'USD', function(rate) {
        expect(jqueryMock.get).toHaveBeenCalledWith(
          exchangeRatesApiUrl,
          { base: 'JPY', symbols: 'USD' },
          jasmine.any(Function)
        );
        expect(rate).toEqual(0.0092139883);

        done();
      });
    });
  });

  describe("refreshRate", function() {
    var response = { rates: { USD: 0.0092139883 } };

    var jqueryMock;
    var rateMock = jasmine.createSpyObj("rateMock", { val: "" });
    var buyCurrencyMock = jasmine.createSpyObj("buyCurrencyMock", [ "val" ]);
    var sellCurrencyMock = jasmine.createSpyObj("sellCurrencyMock", [ "val" ]);

    beforeEach(function() {
      jqueryMock = function(selector) {
        return {
          "#buy-currency": buyCurrencyMock,
          "#sell-currency": sellCurrencyMock,
          "#rate": rateMock
        }[selector]
      }

      jqueryMock.get = function(url, data, callback) { callback(response); };
      spyOn(jqueryMock, "get").and.callThrough();

      rateMock.val.and.returnValue(jasmine.createSpyObj("valReturnValue", [ "change" ]));
    });

    describe("when not both currency fields have values", function() {
      beforeEach(function() {
        buyCurrencyMock.val.and.returnValue("USD");
        sellCurrencyMock.val.and.returnValue("");
      });

      it("resets the rate field", function(done) {
        app(jqueryMock).refreshRate(function() {
          expect(rateMock.val).toHaveBeenCalledWith("");
          done();
        });
      });
    });

    describe("when both currency fields have values", function() {
      beforeEach(function() {
        buyCurrencyMock.val.and.returnValue("USD");
        sellCurrencyMock.val.and.returnValue("JPY");
      });

      it("gets the rate and updates the rate field", function(done) {
        app(jqueryMock).refreshRate(function() {
          expect(rateMock.val).toHaveBeenCalledWith(0.0092139883);
          done();
        });
      });
    });
  });

  describe("refreshBuyAmount", function() {
    var jqueryMock;
    var rateMock = jasmine.createSpyObj("rateMock", [ "val" ]);
    var sellAmountMock = jasmine.createSpyObj("sellAmountMock", [ "val" ]);
    var buyAmountMock = jasmine.createSpyObj("buyAmountMock", [ "val" ]);

    beforeEach(function() {
      jqueryMock = function(selector) {
        return {
          "#buy-amount": buyAmountMock,
          "#rate": rateMock,
          "#sell-amount": sellAmountMock
        }[selector]
      }
    });

    describe("when not both sell amount and rate fields have values", function() {
      beforeEach(function() {
        sellAmountMock.val.and.returnValue("");
        rateMock.val.and.returnValue(1.2345);
      });

      it("resets the buy amount field", function(done) {
        app(jqueryMock).refreshBuyAmount(function() {
          expect(buyAmountMock.val).toHaveBeenCalledWith("");
          done();
        });
      });
    });

    describe("when both sell amount and rate fields have values", function() {
      beforeEach(function() {
        sellAmountMock.val.and.returnValue(10);
        rateMock.val.and.returnValue(1.2345);
      });

      it("calculates the buy amount", function(done) {
        app(jqueryMock).refreshBuyAmount(function() {
          expect(buyAmountMock.val).toHaveBeenCalledWith(12.34);
          done();
        });
      });
    });
  });
});
