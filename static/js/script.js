var app = function($) {
  var exchangeRatesApiUrl = "https://api.exchangeratesapi.io/latest";

  var formatCurrencyCents = function(amountCents, currency) {
    var amount = amountCents / 100;

    return amount.toLocaleString(
      navigator.language,
      {style:"currency", currency: currency}
    );
  };

  var tradesToHtml = function(trades) {
    var tradesHtml = "";
    trades.forEach(function(trade) {
      tradesHtml += '<div class="trades-table-row"><div class="table-body-cell">' +
        trade.sell_currency +
        '</div><div class="table-body-cell">' +
        formatCurrencyCents(trade.sell_amount_cents, trade.sell_currency) +
        '</div><div class="table-body-cell">' +
        trade.buy_currency +
        '</div><div class="table-body-cell">' +
        formatCurrencyCents(trade.buy_amount_cents, trade.buy_currency) +
        '</div><div class="table-body-cell">' +
        trade.rate +
        '</div><div class="table-body-cell">' +
        trade.date_booked +
        '</div></div>';
    });

    return tradesHtml;
  };

  var getTrades = function() {
    $.ajax({
      url: "api/trades",
      success: function(trades) {
        var tradesHtml = tradesToHtml(trades);
        $("#trades-table-body").html(tradesHtml);
      }
    });
  };

  var newTrade = function() {
    $("#new-trade-form").show();
  };

  var hideAndResetForm = function() {
    $("#new-trade-form").hide();
    $("#new-trade-form").trigger("reset");
  };

  var saveTrade = function(event) {
    event.preventDefault();

    request_body = {
      buy_currency: $("#buy-currency").val(),
      sell_currency: $("#sell-currency").val(),
      rate: $("#rate").val(),
      buy_amount_cents: $("#buy-amount").val() * 100,
      sell_amount_cents: $("#sell-amount").val() * 100
    };

    $.post('api/trades', request_body, function() {
      window.alert("New trade created successfully");

      hideAndResetForm();
      getTrades();
    });
  };

  var getRate = function(base, symbol, callback) {
    $.get(
      exchangeRatesApiUrl,
      { base: base, symbols: symbol },
      function(response) {
        // Not clear on the precision required for rates, so I'm leaving it as is
        callback(response["rates"][symbol])
      }
    );
  };

  var refreshBuyAmount = function(callback) {
    var sellAmount = $("#sell-amount").val();
    var rate = $("#rate").val();

    if (sellAmount !== "" && rate !== "") {
      // Quick rounding, but for production use a better strategy for rounding
      // should be used. In fact, JS float handling should be avoided if
      // possible!
      var buyAmount = parseFloat((sellAmount * rate).toFixed(2));

      $("#buy-amount").val(buyAmount);
    } else {
      $("#buy-amount").val("");
    }

    if(callback && typeof(callback) === "function") { callback(); }
  };

  var refreshRate = function(callback) {
    var sellCurrency = $("#sell-currency").val();
    var buyCurrency = $("#buy-currency").val();

    if (!!sellCurrency && !!buyCurrency) {
      getRate(sellCurrency, buyCurrency, function(rate) {
        $("#rate").val(rate).change();
        if(callback && typeof(callback) === "function") { callback(); }
      });
    } else {
      $("#rate").val("").change();
      if(callback && typeof(callback) === "function") { callback(); }
    }
  };

  var init = function() {
    $(document).ready(function(){
      getTrades();

      $("#refresh").click(getTrades);
      $("#new-trade-button").click(newTrade);
      $("#submit-button").click(saveTrade);
      $("#cancel").click(hideAndResetForm);

      $("#sell-currency").change(refreshRate);
      $("#buy-currency").change(refreshRate);

      $("#sell-amount").keyup(refreshBuyAmount);
      $("#rate").change(refreshBuyAmount);
    });
  };

  return {
    formatCurrencyCents: formatCurrencyCents,
    getRate: getRate,
    getTrades: getTrades,
    init: init,
    newTrade: newTrade,
    refreshBuyAmount: refreshBuyAmount,
    refreshRate: refreshRate,
    saveTrade: saveTrade,
    tradesToHtml: tradesToHtml
  };
};

try {
  module.exports = app; // So we can easily test this from Jasmine
} catch (e) {}

