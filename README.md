# Trading Service

This application solves an exercise consisting in creating a Restful API for a trading app, and a browser-based client. It uses a SQLite database for persistence.

### Comments

I decided not to use a single page application framework due to the limited scope of the exercise, and went with plain jQuery instead. This has made testing difficult, but by using dependency injection I could use a jQuery mock that allowed me to follow TDD.

For the backend I decided to use Flask because of its simplicity. I also use SQLite so I could focus on the app itself and not in setting up PostgreSQL (which would have been my choice for a production service) with all the overhead of migrations, docker-compose, etc. The route.py file is only tested via integration tests, and had the application continued to grow, I would have moved most of the route logic to controllers. It's also worth mentioning that I had to change the provider of exchange rate due to fixer.io not allowing to establish a base currency with a free account. It's worth mentioning that I only added three currencies to the select inputs for simplicity: with a bit more time, the complete list can be added outside of the HTML file.

The app lacks any kind of server-side validation. It would be ideal to not rely on the calculations made in the browser (both for security reasons, and for the issues that Javascript presents with rounding), instead keeping those for giving the user a better experience, but making the calculations server-side. Retrieving the exchange rate also should be done server-side. Validation of the currency code can easily be added. Finally, error handling is nonexistent: the server should be able to return proper error messages that can be properly presented to the user.

#### One time env install
```bash
pyenv install 3.7.4
npm install
```

#### Running the app locally

Install dependencies:
```bash
pip install -r requirements.txt
```

Launch the app:
```bash
env FLASK_APP=app flask run
```

OR

Use docker-compose:
```
docker-compose up --build
```

Check the app at http://localhost:5000

#### Running tests locally

Install dependencies:
```bash
pip install -r requirements.dev.txt
```

Run linter:
```bash
# add `-v` to make it more verbose
flake8
```

Run backend test suite:
```bash
pytest
```

Run backend test _and_ linter:
```bash
pytest --flake8
```

Run javascript test suite:
```bash
npx jasmine
```

