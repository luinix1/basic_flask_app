from db import db

from flask import jsonify, request, send_from_directory

from models.trade import Trade


def apply_routes(app):
    @app.route('/')
    @app.route('/index.html')
    def index():
        return app.send_static_file('index.html')

    @app.route('/js/<path:path>')
    def js(path):
        return send_from_directory('static/js',
                                   path,
                                   mimetype='application/javascript')

    @app.route('/api/trades')
    def trades():
        trades = db.session.query(Trade).order_by(Trade.date_booked.desc())\
                .all()
        return jsonify(list(map(lambda trade: trade.to_dict(), trades)))

    @app.route('/api/trades', methods=['POST'])
    def new_trade():
        new_trade = Trade(
            buy_currency=request.form['buy_currency'],
            sell_currency=request.form['sell_currency'],
            buy_amount_cents=request.form['buy_amount_cents'],
            sell_amount_cents=request.form['sell_amount_cents'],
            rate=request.form['rate'],
            )
        db.session.add(new_trade)
        db.session.commit()
        return 'ok'
