import os
basedir = os.path.abspath(os.path.dirname(__file__))


FLASK_ENV = os.getenv('FLASK_ENV', 'development')


class AppConfigBase:
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class AppConfigDevelopment(AppConfigBase):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'dev.db')
    DEBUG = True
    TESTING = False


class AppConfigTest(AppConfigBase):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'test.db')
    DEBUG = False
    TESTING = True


config = {
    'development': AppConfigDevelopment,
    'test': AppConfigTest,
}
