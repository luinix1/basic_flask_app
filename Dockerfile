FROM python:3.7-alpine

ENV FLASK_APP=app

WORKDIR /app
COPY requirements.txt /app
RUN pip install --upgrade pip && pip install --no-cache-dir -r /app/requirements.txt

COPY . /app

ENTRYPOINT ["flask"]

EXPOSE 5000
CMD ["run", "--host", "0.0.0.0"]
