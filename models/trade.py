import random
import string

from db import db

from sqlalchemy.sql import func

from sqlalchemy_serializer import SerializerMixin


# Using some type of hash function that could better avoid collisions would be
# desirable
def trade_id(length=7):
    letters_digits = string.ascii_uppercase + string.digits
    return 'TR' + ''.join(random.choice(letters_digits) for i in range(length))


class Trade(db.Model, SerializerMixin):
    # Not setting a length limit on purpose. Storage is cheap, but future
    # migrations can be very painful.
    id = db.Column(db.String, default=trade_id, primary_key=True)

    # On the other hand, ISO_4217 guarantees 3-digit currency codes
    buy_currency = db.Column(db.String(3), nullable=False)
    sell_currency = db.Column(db.String(3), nullable=False)

    # Not clear on whether it's worth specifying this as a BigInteger
    # In sqlite, it shouldn't be needed.
    buy_amount_cents = db.Column(db.Integer, nullable=False)
    sell_amount_cents = db.Column(db.Integer, nullable=False)

    rate = db.Column(db.Float, nullable=False)

    # It's a good practice to delegate timestamp generation to the database
    date_booked = db.Column(
            db.DateTime(timezone=True),
            server_default=func.now(),
            nullable=False)
